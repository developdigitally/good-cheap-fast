//console.clear();

var uncheck = function (el){
  console.log(el);
  $(el).prop('checked', false);
  $(el).parent().removeClass('active');
};

var checkOptions = function(optionTry){
  var good = $('#good')[0].checked;
  var cheap = $('#cheap')[0].checked;
  var fast = $('#fast')[0].checked;
  var option = optionTry;
  
  if (good && option == 'cheap') {
      uncheck('#fast');
    
  } else if (cheap && option == 'fast') {
      uncheck('#good');
          
  } else if (fast && option == "good") {
    uncheck('#cheap');     
  }
  //console.log(optionTry);
  //console.log(good, cheap, fast);
}

$(function(){
  $(':radio').on('change', function(){
    var id = $(this)[0].id;
    $(this).parent().addClass('active');
    checkOptions(id);
  });
});